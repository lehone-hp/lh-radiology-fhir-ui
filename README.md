# LhRadiologyFhirUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Prerequisite
  * lh-radiology-fhir server
  * nodejs

## Configuration

Open the application configuration file at src/assets/config/config.json and change the value of 
`ohif.url` and `apiserver.restUrl` to the corresponding URLs of the OHIF viewer and the FHIR REST server. Note that the 
URLs should not end with a backslash(/)

The default `ohif.url` is `http://localhost:3000/viewer`

The default `apiServer.restUrl` is `http://localhost:8087/fhir`
## Run

Clone repository from github

  `git clone https://gitlab.com/lehone-hp/lh-radiology-fhir-ui.git`
  
Change to project home directory

  `cd lh-radiology-fhir`
  
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
