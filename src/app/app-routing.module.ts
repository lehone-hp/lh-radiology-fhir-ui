import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PatientRegistrationComponent} from './components/pages/patient/patient-registration/patient-registration.component';
import {PatientSearchComponent} from './components/pages/patient/patient-registration/patient-search/patient-search.component';
import {PatientDashboardComponent} from './components/pages/patient/patient-dashboard/patient-dashboard.component';
import {ApplicationHomeComponent} from './components/application-home/application-home.component';
import {ProcedureRequest} from './models/procedure-request';
import {ProcedureRequestComponent} from './components/procedure-request/procedure-request.component';
import {ImagingStudyComponent} from './components/imaging-study/imaging-study.component';
import {EditProcedureRequestComponent} from './components/procedure-request/edit-procedure-request/edit-procedure-request.component';
import {ImagingStudyDetailComponent} from './components/imaging-study/imaging-study-detail/imaging-study-detail.component';
import {ProcedureRequestDetailComponent} from './components/procedure-request/procedure-request-detail/procedure-request-detail.component';
import {ReportEditorComponent} from './components/report/report-editor/report-editor.component';
import {ReportComponent} from './components/report/report.component';
import {ReportViewerComponent} from './components/report/report-viewer/report-viewer.component';
import {ReadingListPageComponent} from './components/pages/reading-list-page/reading-list-page.component';
import {ProcedureRequestDetailPageComponent} from './components/pages/procedure-request-detail-page/procedure-request-detail-page.component';
import {ProcedureRequestPageComponent} from './components/pages/procedure-request-page/procedure-request-page.component';
import {ReportEditorPageComponent} from './components/pages/report-editor-page/report-editor-page.component';
import {ReportViewPageComponent} from './components/pages/report-view-page/report-view-page.component';
import {ReportPageComponent} from './components/pages/report-page/report-page.component';

const routes: Routes = [
  { path: '', component: ApplicationHomeComponent},
  { path: 'patient-registration', component: PatientRegistrationComponent },
  { path: 'patient-registration/search', component: PatientSearchComponent},
  { path: 'patient-dashboard/patient/:id', component: PatientDashboardComponent },

  { path: 'orders', component: ProcedureRequestPageComponent},
  { path: 'orders/edit/:id', component: EditProcedureRequestComponent},
  { path: 'orders/:id', component: ProcedureRequestDetailPageComponent},

  { path: 'reading-list', component: ReadingListPageComponent},

  { path: 'report/:report-id', component: ReportViewPageComponent},
  { path: 'report/edit/:report-id', component: ReportEditorPageComponent},
  { path: 'reports', component: ReportPageComponent},

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
