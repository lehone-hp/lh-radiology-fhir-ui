import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';


import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { PatientRegistrationComponent } from './components/pages/patient/patient-registration/patient-registration.component';
import { PatientSearchComponent } from './components/pages/patient/patient-registration/patient-search/patient-search.component';
import { PatientService } from './services/patient.service';
import { HttpClientModule } from '@angular/common/http';
import { PatientDashboardComponent } from './components/pages/patient/patient-dashboard/patient-dashboard.component';
import { ProcedureRequestComponent } from './components/procedure-request/procedure-request.component';
import {ProcedureRequestService} from './services/procedure-request.service';
import {FormsModule} from '@angular/forms';
import { BaseService } from './services/base-service/base.service';
import { PractitionerService } from './services/practitioner.service';
import { ApplicationHomeComponent } from './components/application-home/application-home.component';
import {ToastModule} from 'ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ImagingStudyComponent } from './components/imaging-study/imaging-study.component';
import {ImagingStudyService} from './services/imaging-study.service';
import { AddProcedureRequestComponent } from './components/procedure-request/add-procedure-request/add-procedure-request.component';
import { EditProcedureRequestComponent } from './components/procedure-request/edit-procedure-request/edit-procedure-request.component';
import { ImagingStudyDetailComponent } from './components/imaging-study/imaging-study-detail/imaging-study-detail.component';
import { ProcedureRequestDetailComponent } from './components/procedure-request/procedure-request-detail/procedure-request-detail.component';
import { ReportEditorComponent } from './components/report/report-editor/report-editor.component';
import {DiagnosticReportService} from './services/diagnostic-report.service';
import { ReportComponent } from './components/report/report.component';
import { ReportViewerComponent } from './components/report/report-viewer/report-viewer.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import {AppConfigService} from './services/config/app-config.service';
import {AppConfig} from './models/config/app-config';
import {OHIFUtil} from './utils/OHIFUtil';
import { ReadingListPageComponent } from './components/pages/reading-list-page/reading-list-page.component';
import { ProcedureRequestDetailPageComponent } from './components/pages/procedure-request-detail-page/procedure-request-detail-page.component';
import { ProcedureRequestPageComponent } from './components/pages/procedure-request-page/procedure-request-page.component';
import { ReportEditorPageComponent } from './components/pages/report-editor-page/report-editor-page.component';
import { ReportViewPageComponent } from './components/pages/report-view-page/report-view-page.component';
import { ReportPageComponent } from './components/pages/report-page/report-page.component';

export function initializeApp(appConfig: AppConfigService) {
  return () => appConfig.load();
}

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    PatientRegistrationComponent,
    PatientSearchComponent,
    PatientDashboardComponent,
    ProcedureRequestComponent,
    ApplicationHomeComponent,
    ImagingStudyComponent,
    AddProcedureRequestComponent,
    EditProcedureRequestComponent,
    ImagingStudyDetailComponent,
    ProcedureRequestDetailComponent,
    ReportEditorComponent,
    ReportComponent,
    ReportViewerComponent,
    ReadingListPageComponent,
    ProcedureRequestDetailPageComponent,
    ProcedureRequestPageComponent,
    ReportEditorPageComponent,
    ReportViewPageComponent,
    ReportPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    EditorModule
  ],
  providers: [
    PatientService,
    ProcedureRequestService,
    BaseService,
    PractitionerService,
    ImagingStudyService,
    DiagnosticReportService,
    AppConfigService,
    { provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfigService], multi: true },
    OHIFUtil,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
