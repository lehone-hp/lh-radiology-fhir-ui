import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagingStudyDetailComponent } from './imaging-study-detail.component';

describe('ImagingStudyDetailComponent', () => {
  let component: ImagingStudyDetailComponent;
  let fixture: ComponentFixture<ImagingStudyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagingStudyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagingStudyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
