import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-imaging-study-detail',
  templateUrl: './imaging-study-detail.component.html',
  styleUrls: ['./imaging-study-detail.component.css']
})
export class ImagingStudyDetailComponent implements OnInit {

  @Input()
  url: SafeResourceUrl;

  constructor(public sanitizer : DomSanitizer) { }

  ngOnInit() {
    //TODO pass url as attribute
    //this.url = this.sanitizer.bypassSecurityTrustResourceUrl('http://localhost:3000/viewer/1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.50.2.605');
    console.log(`From detail component ${this.url}`);
  }

}
