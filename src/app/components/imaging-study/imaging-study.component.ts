import {Component, Input, OnInit} from '@angular/core';
import {ImagingStudy} from '../../models/imaging-study';
import {ImagingStudyService} from '../../services/imaging-study.service';
import {Router} from '@angular/router';
import {ProcedureRequestService} from '../../services/procedure-request.service';
import {ProcedureRequest} from '../../models/procedure-request';
import {DomSanitizer} from '@angular/platform-browser';
import {DiagnosticReportService} from '../../services/diagnostic-report.service';
import {DiagnosticReport} from '../../models/diagnostic-report';
import {Reference, ReportPerformer} from '../../models/common';
import {OHIFUtil} from '../../utils/OHIFUtil';

@Component({
  selector: 'app-imaging-study',
  templateUrl: './imaging-study.component.html',
  styleUrls: ['./imaging-study.component.css']
})
export class ImagingStudyComponent implements OnInit {

  @Input()
  studies: ImagingStudy[];

  router: Router;

  constructor(private studyService: ImagingStudyService,
              private procedureRequestService: ProcedureRequestService,
              private reportService: DiagnosticReportService,
              private _router: Router,
              private sanitizer : DomSanitizer,
              public ohifUtil: OHIFUtil) {
    this.router = this._router;
  }

  ngOnInit() {
  }

  claimStudy(studyId: string): void {
    // get the imaging study
    this.studyService.getImagingStudyById(studyId)
      .subscribe(study => {
        if (study.basedOn[0]) {

          // get the corresponding procedure request
          this.procedureRequestService.getProcedureRequestById(study.basedOn[0].reference)
            .subscribe(pr => {
              let procedureRequest: ProcedureRequest = Object.assign(new ProcedureRequest(), pr);
              console.log(pr);

              // change the request's status to completed
              procedureRequest.status = 'completed';

              this.procedureRequestService.updateProcedureRequest(procedureRequest.id, procedureRequest)
                .subscribe(result => {

                  // create new report with status draft
                  const report = new DiagnosticReport();
                  console.log(report);
                  report.resourceType = 'DiagnosticReport';
                  report.status = 'preliminary';
                  report.basedOn = study.basedOn;
                  report.subject = study.patient;
                  report.issued = new Date();

                  let studyRef: Reference = new Reference();
                  studyRef.reference = `ImagingStudy/${study.id}`;
                  report.imagingStudy[0] = studyRef;

                  let performer = new ReportPerformer();
                  performer.actor.reference = study.referrer.reference;
                  report.performer.push(performer);

                  this.reportService.createReport(report)
                    .subscribe(resp => {
                      console.log(`Report response ${resp}`);

                      this.reportService.getReportByImagingStudy(study.id)
                        .subscribe(reportBundle => {
                          let report = Object.assign(new DiagnosticReport(), reportBundle.entry[0].resource);
                          console.log('Returned Report');
                          console.log(report);
                          this.router.navigate([`/report/edit/${report.id}`]);
                        });
                    })
                });
            });
        }
      });
  }

  /* Helper Methods*/

  // Opens modal to show procedure request for editing
  showModal(modalId: string): void {
    document.getElementById(modalId).style.display='block';
    document.getElementById(modalId).classList.add('in');
  }

  // Opens modal to show procedure request for editing
  closeModal(modalId: string): void {
    document.getElementById(modalId).style.display='none';
    document.getElementById(modalId).classList.remove('in');
  }
}
