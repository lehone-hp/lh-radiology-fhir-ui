import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../../models/patient';
import {PatientService} from '../../../../services/patient.service';
import {ActivatedRoute} from '@angular/router';
import {ImagingStudy} from '../../../../models/imaging-study';
import {ImagingStudyService} from '../../../../services/imaging-study.service';
import {PractitionerService} from '../../../../services/practitioner.service';
import {Practitioner} from '../../../../models/practitioner';
import {ProcedureRequest} from '../../../../models/procedure-request';
import {ProcedureRequestService} from '../../../../services/procedure-request.service';
import {DiagnosticReport} from '../../../../models/diagnostic-report';
import {DiagnosticReportService} from '../../../../services/diagnostic-report.service';

@Component({
  selector: 'app-patient-dashboard',
  templateUrl: './patient-dashboard.component.html',
  styleUrls: ['./patient-dashboard.component.css']
})
export class PatientDashboardComponent implements OnInit {

  patient: Patient;

  procedures: ProcedureRequest[];

  studies: ImagingStudy[];

  reports: DiagnosticReport[];

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientService,
    private studyService: ImagingStudyService,
    private practitionerService: PractitionerService,
    private procedureRequestService: ProcedureRequestService,
    private reportService: DiagnosticReportService
  ) { }

  ngOnInit() {
    this.getPatient();
  }

  getPatient(): void {
    // retrieve the id of the patient from the route
    const patientId = this.route.snapshot.paramMap.get('id');
    this.patientService.getPatientById(patientId)
      .subscribe(patient => {
        this.patient = Object.assign(new Patient(), patient);
        console.log(patient);

        this.getProceduresByPatient(this.patient.id);
        this.getImagingStudiesByPatient(this.patient.id);
        this.getReportsByPatient(this.patient.id);
      });
  }

  getProceduresByPatient(patientId: string): void {
    this.procedures = [];

    this.procedureRequestService.getProceduresByPatientId(patientId).subscribe(bundle => {
      for (const entry of bundle.entry) {
        const next: ProcedureRequest = Object.assign(new ProcedureRequest(), entry.resource);

        // get the patient and then the patient's name
        this.patientService.getPatientById(next.subject.reference)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            next.patientName = patient.getPersonName();
          });

        // get the requester physician's name
        this.practitionerService.getPractitionerById(next.requester.agent.reference)
          .subscribe(resp => {
            const requester: Practitioner = Object.assign(new Practitioner(), resp);
            next.requesterName = requester.getPersonName();
          });

        // get the performer's name
        this.practitionerService.getPractitionerById(next.performer.reference)
          .subscribe(resp => {
            const performer: Practitioner = Object.assign(new Practitioner(), resp);
            next.performerName = performer.getPersonName();
          });

        this.procedures.push(next);
      }
    });

    console.log(this.procedures);
  }

  getImagingStudiesByPatient(patientId: string): void {
    this.studies = [];

    this.studyService.getImagingStudyByPatientId(patientId).subscribe(bundle => {
      for (const entry of bundle.entry) {

        console.log(entry);
        const next: ImagingStudy = Object.assign(new ImagingStudy(), entry.resource);

        // get the patient and then the patient's name
        this.patientService.getPatientById(next.patient.reference)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            next.patientName = patient.getPersonName();
          });

        // get the referring physician's name
        this.practitionerService.getPractitionerById(next.referrer.reference)
          .subscribe(resp => {
            const referrer: Practitioner = Object.assign(new Practitioner(), resp);
            next.referrerName = referrer.getPersonName();
          });

        // get the corresponding ProcedureRequest
        if (next.basedOn[0]) {
          this.procedureRequestService.getProcedureRequestById(next.basedOn[0].reference)
            .subscribe(resp => {
              console.log(resp);
              const procedureRequest: ProcedureRequest = Object.assign(new ProcedureRequest(), resp);
              next.procedureRequest = procedureRequest;
            });
        }
        this.studies.push(next);

      }
      console.log(this.studies);
    });
  }

  getReportsByPatient(patientId: string): void {
    this.reports = [];

    this.reportService.getReportByPatientId(patientId)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          const next: DiagnosticReport = Object.assign(new DiagnosticReport(), entry.resource);

          this.reports.push(next);
        }

        console.log(this.reports);
      });

  }
}
