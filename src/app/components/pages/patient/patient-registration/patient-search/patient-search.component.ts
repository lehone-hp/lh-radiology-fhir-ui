import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../../../models/patient';
import {PatientService} from '../../../../../services/patient.service';

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.css']
})
export class PatientSearchComponent implements OnInit {

  patients: Patient[] = [];
  q = '';

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.getPatients();
  }

  getPatients(): void {
    this.patients = [];
    this.patientService.searchPatientsByName(this.q)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          this.patients.push(entry.resource);
        }
        console.log(bundle);
        console.log(this.patients);
      });
  }
}
