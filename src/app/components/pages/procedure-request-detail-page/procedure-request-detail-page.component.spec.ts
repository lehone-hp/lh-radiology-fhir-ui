import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureRequestDetailPageComponent } from './procedure-request-detail-page.component';

describe('ProcedureRequestDetailPageComponent', () => {
  let component: ProcedureRequestDetailPageComponent;
  let fixture: ComponentFixture<ProcedureRequestDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureRequestDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureRequestDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
