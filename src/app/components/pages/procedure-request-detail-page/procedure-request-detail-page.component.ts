import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {Practitioner} from '../../../models/practitioner';
import {ProcedureRequest} from '../../../models/procedure-request';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PatientService} from '../../../services/patient.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';
import {ActivatedRoute} from '@angular/router';
import {ToastsManager} from 'ng2-toastr';
import {Patient} from '../../../models/patient';

@Component({
  selector: 'app-procedure-request-detail-page',
  templateUrl: './procedure-request-detail-page.component.html',
  styleUrls: ['./procedure-request-detail-page.component.css']
})
export class ProcedureRequestDetailPageComponent implements OnInit {

  procedureRequest: ProcedureRequest;

  // report associated to this request
  report: DiagnosticReport;

  constructor(private procedureRequestService: ProcedureRequestService,
              private practitionerService: PractitionerService,
              private patientService: PatientService,
              private reportService: DiagnosticReportService,
              private route: ActivatedRoute,
              private toaster: ToastsManager, vcr: ViewContainerRef) {
    this.toaster.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    const procedureRequestId = this.route.snapshot.paramMap.get('id');

    this.procedureRequestService.getProcedureRequestById(procedureRequestId)
      .subscribe(resp => {
        this.procedureRequest = Object.assign(new ProcedureRequest(), resp);

        // get the patient and then the patient's name
        this.patientService.getPatientById(this.procedureRequest.subject.reference)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            this.procedureRequest.patientName = patient.getPersonName();
          });

        // get the requester physician's name
        this.practitionerService.getPractitionerById(this.procedureRequest.requester.agent.reference)
          .subscribe(resp => {
            const requester: Practitioner = Object.assign(new Practitioner(), resp);
            this.procedureRequest.requesterName = requester.getPersonName();
          });

        // get the performer's name
        this.practitionerService.getPractitionerById(this.procedureRequest.performer.reference)
          .subscribe(resp => {
            const performer: Practitioner = Object.assign(new Practitioner(), resp);
            this.procedureRequest.performerName = performer.getPersonName();
          });

        // get the report associated to this report if available
        this.reportService.getReportByProcedureRequest(this.procedureRequest.id)
          .subscribe(reportBundle => {
            this.report = Object.assign(new DiagnosticReport(), reportBundle.entry[0].resource);
            console.log("The ProcedureRequest's Report");
            console.log(this.report);
          });
        console.log(this.procedureRequest )

      });

  }

}
