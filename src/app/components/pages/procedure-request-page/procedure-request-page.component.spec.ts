import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureRequestPageComponent } from './procedure-request-page.component';

describe('ProcedureRequestPageComponent', () => {
  let component: ProcedureRequestPageComponent;
  let fixture: ComponentFixture<ProcedureRequestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureRequestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureRequestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
