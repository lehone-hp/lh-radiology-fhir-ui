import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../models/patient';
import {ProcedureRequest} from '../../../models/procedure-request';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {PatientService} from '../../../services/patient.service';
import {Practitioner} from '../../../models/practitioner';

@Component({
  selector: 'app-procedure-request-page',
  templateUrl: './procedure-request-page.component.html',
  styleUrls: ['./procedure-request-page.component.css']
})
export class ProcedureRequestPageComponent implements OnInit {

  procedures: ProcedureRequest[] = [];

  constructor(private procedureRequestService: ProcedureRequestService,
              private practitionerService: PractitionerService,
              private patientService: PatientService) {
  }

  ngOnInit() {
    this.getProcedures();
  }

  /**
   * Query the server and get all previous procedure requests.
   */
  getProcedures(): void {
    this.procedures = [];

    this.procedureRequestService.getAllProcedureRequests().subscribe(bundle => {
      for (const entry of bundle.entry) {
        const next: ProcedureRequest = Object.assign(new ProcedureRequest(), entry.resource);

        // get the patient and then the patient's name
        this.patientService.getPatientById(next.subject.reference)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            next.patientName = patient.getPersonName();
          });

        // get the requester physician's name
        this.practitionerService.getPractitionerById(next.requester.agent.reference)
          .subscribe(resp => {
            const requester: Practitioner = Object.assign(new Practitioner(), resp);
            next.requesterName = requester.getPersonName();
          });

        // get the performer's name
        this.practitionerService.getPractitionerById(next.performer.reference)
          .subscribe(resp => {
            const performer: Practitioner = Object.assign(new Practitioner(), resp);
            next.performerName = performer.getPersonName();
          });

        this.procedures.push(next);
      }
    });

    console.log(this.procedures);
  }
}
