import { Component, OnInit } from '@angular/core';
import {ImagingStudy} from '../../../models/imaging-study';
import {ActivatedRoute} from '@angular/router';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {PatientService} from '../../../services/patient.service';
import {ImagingStudyService} from '../../../services/imaging-study.service';
import {Observable} from 'rxjs/Observable';
import {ImagingStudyBundle} from '../../../models/bundles/imaging-study-bundle';
import {Practitioner} from '../../../models/practitioner';
import {Patient} from '../../../models/patient';
import {ProcedureRequest} from '../../../models/procedure-request';

@Component({
  selector: 'app-reading-list-page',
  templateUrl: './reading-list-page.component.html',
  styleUrls: ['./reading-list-page.component.css']
})
export class ReadingListPageComponent implements OnInit {

  studies: ImagingStudy[] = [];

  constructor(private studyService: ImagingStudyService,
              private patientService: PatientService,
              private practitionerService: PractitionerService,
              private procedureRequestService: ProcedureRequestService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    const patientId = this.route.snapshot.paramMap.get('id');
    this.getImagingStudies(patientId);
  }

  getImagingStudies(patientId: string): void {
    this.studies = [];
    let response : Observable<ImagingStudyBundle>;

    if (patientId) {
      response = this.studyService.getImagingStudyByPatientId(patientId);
    } else {
      response = this.studyService.getImagingStudies();
    }

    response.subscribe(bundle => {
      for (const entry of bundle.entry) {

        console.log(entry);
        const next: ImagingStudy = Object.assign(new ImagingStudy(), entry.resource);

        // get the patient and then the patient's name
        this.patientService.getPatientById(next.patient.reference)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            next.patientName = patient.getPersonName();
          });

        // get the referring physician's name
        this.practitionerService.getPractitionerById(next.referrer.reference)
          .subscribe(resp => {
            const referrer: Practitioner = Object.assign(new Practitioner(), resp);
            next.referrerName = referrer.getPersonName();
          });

        // get the corresponding ProcedureRequest
        if (next.basedOn[0]) {
          this.procedureRequestService.getProcedureRequestById(next.basedOn[0].reference)
            .subscribe(resp => {
              console.log(resp);
              const procedureRequest: ProcedureRequest = Object.assign(new ProcedureRequest(), resp);
              next.procedureRequest = procedureRequest;
            });
        }
        this.studies.push(next);

      }
      console.log(this.studies);
    });
  }

}
