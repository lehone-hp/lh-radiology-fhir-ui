import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportEditorPageComponent } from './report-editor-page.component';

describe('ReportEditorPageComponent', () => {
  let component: ReportEditorPageComponent;
  let fixture: ComponentFixture<ReportEditorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportEditorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportEditorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
