import { Component, OnInit } from '@angular/core';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {Patient} from '../../../models/patient';
import {ImagingStudy} from '../../../models/imaging-study';
import {Practitioner} from '../../../models/practitioner';
import {ActivatedRoute, Router} from '@angular/router';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {ImagingStudyService} from '../../../services/imaging-study.service';
import {PatientService} from '../../../services/patient.service';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';
import {OHIFUtil} from '../../../utils/OHIFUtil';
import {Code} from '../../../models/common';

@Component({
  selector: 'app-report-editor-page',
  templateUrl: './report-editor-page.component.html',
  styleUrls: ['./report-editor-page.component.css']
})
export class ReportEditorPageComponent implements OnInit {

  report: DiagnosticReport;

  patient: Patient;

  study: ImagingStudy;

  performer: Practitioner;

  reportText: string;

  router: Router;

  constructor(private practitionerService: PractitionerService,
              private studyService: ImagingStudyService,
              private patientService: PatientService,
              private reportService: DiagnosticReportService,
              private route: ActivatedRoute,
              private _router: Router,
              public ohifUtil: OHIFUtil) {
    this.router = this._router;
  }

  ngOnInit() {
    const reportId = this.route.snapshot.paramMap.get('report-id');
    console.log(reportId);

    this.reportService.getReportById(reportId)
      .subscribe(resp => {
        this.report = resp;

        this.patientService.getPatientById(this.report.subject.reference)
          .subscribe(patient => {
            this.patient = Object.assign(new Patient(), patient);
            console.log(patient);
          });

        this.studyService.getImagingStudyById(this.report.imagingStudy[0].reference)
          .subscribe(study => {
            this.study = Object.assign(new ImagingStudy(), study);
            console.log(study);
          });

        this.practitionerService.getPractitionerById(this.report.performer[0].actor.reference)
          .subscribe(performer => {
            this.performer = Object.assign(new Practitioner(), performer);
            console.log(performer);
          });

        // If report has been started
        if (this.report.codedDiagnosis) {
          this.reportText = this.report.codedDiagnosis[0].text;
        }
      });
  }

}
