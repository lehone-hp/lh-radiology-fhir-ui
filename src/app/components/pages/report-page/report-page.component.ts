import { Component, OnInit } from '@angular/core';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';

@Component({
  selector: 'app-report-page',
  templateUrl: './report-page.component.html',
  styleUrls: ['./report-page.component.css']
})
export class ReportPageComponent implements OnInit {

  reports: DiagnosticReport[] = [];

  constructor(private reportService: DiagnosticReportService) { }

  ngOnInit() {
    this.getReports();
  }

  /**
   * Query the server and get all previous diagnostic reports.
   */
  getReports(): void {
    this.reports = [];

    this.reportService.getAllReports()
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          const next: DiagnosticReport = Object.assign(new DiagnosticReport(), entry.resource);

          this.reports.push(next);
        }

        console.log(this.reports);
      });

  }
}
