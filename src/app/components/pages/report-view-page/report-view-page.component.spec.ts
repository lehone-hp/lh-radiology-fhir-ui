import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportViewPageComponent } from './report-view-page.component';

describe('ReportViewPageComponent', () => {
  let component: ReportViewPageComponent;
  let fixture: ComponentFixture<ReportViewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportViewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
