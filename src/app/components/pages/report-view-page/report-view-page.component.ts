import { Component, OnInit } from '@angular/core';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {Patient} from '../../../models/patient';
import {ImagingStudy} from '../../../models/imaging-study';
import {ProcedureRequest} from '../../../models/procedure-request';
import {Practitioner} from '../../../models/practitioner';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {OHIFUtil} from '../../../utils/OHIFUtil';
import {ActivatedRoute} from '@angular/router';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';
import {PatientService} from '../../../services/patient.service';
import {ImagingStudyService} from '../../../services/imaging-study.service';

@Component({
  selector: 'app-report-view-page',
  templateUrl: './report-view-page.component.html',
  styleUrls: ['./report-view-page.component.css']
})
export class ReportViewPageComponent implements OnInit {

  report: DiagnosticReport;

  patient: Patient;

  study: ImagingStudy;

  procedureRequest: ProcedureRequest;

  performer: Practitioner;

  constructor(private procedureRequestService: ProcedureRequestService,
              private practitionerService: PractitionerService,
              private studyService: ImagingStudyService,
              private patientService: PatientService,
              private reportService: DiagnosticReportService,
              private route: ActivatedRoute,
              public ohifUtil: OHIFUtil) { }

  ngOnInit() {
    const reportId = this.route.snapshot.paramMap.get('report-id');
    console.log(reportId);

    this.reportService.getReportById(reportId)
      .subscribe(resp => {
        this.report = resp;

        this.patientService.getPatientById(this.report.subject.reference)
          .subscribe(patient => {
            this.patient = Object.assign(new Patient(), patient);
            console.log(patient);
          });

        this.studyService.getImagingStudyById(this.report.imagingStudy[0].reference)
          .subscribe(study => {
            this.study = Object.assign(new ImagingStudy(), study);
            console.log(study);
          });

        this.practitionerService.getPractitionerById(this.report.performer[0].actor.reference)
          .subscribe(performer => {
            this.performer = Object.assign(new Practitioner(), performer);
            console.log(performer);
          });

        this.procedureRequestService.getProcedureRequestById(this.report.basedOn[0].reference)
          .subscribe(request => {
            this.procedureRequest = Object.assign(new ProcedureRequest(), request);

            // get the patient and then the patient's name
            this.patientService.getPatientById(this.procedureRequest.subject.reference)
              .subscribe(resp => {
                const patient: Patient = Object.assign(new Patient(), resp);
                this.procedureRequest.patientName = patient.getPersonName();
              });

            // get the requester physician's name
            this.practitionerService.getPractitionerById(this.procedureRequest.requester.agent.reference)
              .subscribe(resp => {
                const requester: Practitioner = Object.assign(new Practitioner(), resp);
                this.procedureRequest.requesterName = requester.getPersonName();
              });

            // get the performer's name
            this.practitionerService.getPractitionerById(this.procedureRequest.performer.reference)
              .subscribe(resp => {
                const performer: Practitioner = Object.assign(new Practitioner(), resp);
                this.procedureRequest.performerName = performer.getPersonName();
              });
          });
      });
  }
}
