import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProcedureRequestComponent } from './add-procedure-request.component';

describe('AddProcedureRequestComponent', () => {
  let component: AddProcedureRequestComponent;
  let fixture: ComponentFixture<AddProcedureRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProcedureRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProcedureRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
