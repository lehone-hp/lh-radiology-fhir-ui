import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Annotation, Coding} from '../../../models/common';
import {BodySite, ProcedureRequest} from '../../../models/procedure-request';
import {Practitioner} from '../../../models/practitioner';
import {Patient} from '../../../models/patient';
import {ToastsManager} from 'ng2-toastr';
import {PatientService} from '../../../services/patient.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {ProcedureRequestFormModel} from '../../../models/forms/procedure-request-form-model';

@Component({
  selector: 'app-add-procedure-request',
  templateUrl: './add-procedure-request.component.html',
  styleUrls: ['./add-procedure-request.component.css']
})
export class AddProcedureRequestComponent implements OnInit {

  requestForm = new ProcedureRequestFormModel();

  practitioners: Practitioner[] = [];

  patients: Patient[] = [];

  constructor(private procedureRequestService: ProcedureRequestService,
              private practitionerService: PractitionerService,
              private patientService: PatientService,
              private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  // used to search patient when creating a new ProcedureRequest
  getPatientsByName(name: string): void {
    this.patients = [];
    this.patientService.searchPatientsByName(name)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          const next: Patient = Object.assign(new Patient(), entry.resource);
          this.patients.push(next);
        }
        console.log(this.patients);
      });
  }

  getPractitionersByName(name: string): void {
    this.practitioners = [];
    this.practitionerService.getPractitionerByName(name)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          //  Push each entry as an Object so as  to access built in class methods
          const next: Practitioner = Object.assign(new Practitioner(), entry.resource)
          this.practitioners.push(next);
        }
        console.log(this.practitioners);
      });
  }

  /**
   * Creates a new ProcedureRequest with input values from teh forms and POST thid
   * ProcedureRequest to the serer for storage.
   */
  onNewProcedureFormSubmit(): void {
    console.log('Input values from Form')
    console.log(this.requestForm);
    const procedureRequest = new ProcedureRequest();
    procedureRequest.status = 'active';
    procedureRequest.intent = this.requestForm.intent;
    procedureRequest.authoredOn = new Date();
    procedureRequest.priority  = this.requestForm.priority;

    this.practitionerService.getPractitionerById(this.requestForm.performer)
      .subscribe(ret => {
        const performer = Object.assign(new Practitioner(), ret);

        procedureRequest.performer.reference = `Practitioner/${performer.id}`;
        procedureRequest.performer.display = performer.getPersonName();
        // assume that requester is the performer
        procedureRequest.requester.agent.reference = `Practitioner/${performer.id}`;
        procedureRequest.requester.agent.display = performer.getPersonName();
        procedureRequest.subject.reference = `Patient/${this.requestForm.patient}`;
        this.patientService.getPatientById(this.requestForm.patient)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            procedureRequest.subject.display = patient.getPersonName();
            console.log("Patients Name: " + patient.getPersonName());
          });
        procedureRequest.doNotPerform = false;
        procedureRequest.occurrenceDateTime = this.requestForm.occurrenceDate;

        // Body Site
        const bodySite = new BodySite();
        bodySite.text = this.requestForm.bodySite;
        procedureRequest.bodySite = [];
        procedureRequest.bodySite.push(bodySite);

        // Coding - set Modality
        const coding = new Coding();
        coding.code = this.requestForm.modality;
        coding.display = 'Modality';
        coding.system = 'http://hl7.org/fhir/v2/0259';
        procedureRequest.code.coding.push(coding);
        procedureRequest.code.text = 'Modality';

        // Note
        const note = new Annotation();
        note.text = this.requestForm.note;
        procedureRequest.note.push(note);

        console.log('Posting procedure-request to server');
        console.log(procedureRequest);

        this.procedureRequestService.createProcedureRequest(procedureRequest)
          .subscribe(message => {
            console.log(message);
            this.toastr.success('Procedure Request created.', 'Success!');
            window.location.reload();
          });
      });
  }
}
