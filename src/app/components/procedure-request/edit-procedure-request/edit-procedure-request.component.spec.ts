import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProcedureRequestComponent } from './edit-procedure-request.component';

describe('EditProcedureRequestComponent', () => {
  let component: EditProcedureRequestComponent;
  let fixture: ComponentFixture<EditProcedureRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProcedureRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProcedureRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
