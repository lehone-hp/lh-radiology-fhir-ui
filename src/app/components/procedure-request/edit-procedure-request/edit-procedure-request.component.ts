import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {BodySite, ProcedureRequestPost} from '../../../models/procedure-request';
import {ProcedureRequestFormModel} from '../../../models/forms/procedure-request-form-model';
import {PatientService} from '../../../services/patient.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {Patient} from '../../../models/patient';
import {Practitioner} from '../../../models/practitioner';
import {ToastsManager} from 'ng2-toastr';
import {Annotation, Coding} from '../../../models/common';

@Component({
  selector: 'app-edit-procedure-request',
  templateUrl: './edit-procedure-request.component.html',
  styleUrls: ['./edit-procedure-request.component.css']
})
export class EditProcedureRequestComponent implements OnInit {


  @Input() theProcedureRequest: ProcedureRequestPost;

  requestForm = new ProcedureRequestFormModel();//'', '', '', '', '', '', '', '', new Date());

  practitioners: Practitioner[] = [];

  patients: Patient[] = [];

  constructor(private procedureRequestService: ProcedureRequestService,
              private practitionerService: PractitionerService,
              private patientService: PatientService,
              private toaster: ToastsManager, vcr: ViewContainerRef) {
    this.toaster.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
        console.log("The Procedure Request");
        console.log(this.theProcedureRequest);

        this.requestForm.patient = this.theProcedureRequest.subject.reference;
        this.requestForm.performer = this.theProcedureRequest.performer.reference;
        this.requestForm.modality = this.theProcedureRequest.code.coding[0].code;
        this.requestForm.bodySite = this.theProcedureRequest.bodySite[0].text;
        this.requestForm.priority = this.theProcedureRequest.priority;
        this.requestForm.intent = this.theProcedureRequest.intent;
        this.requestForm.status = this.theProcedureRequest.status;
        this.requestForm.occurrenceDate = this.theProcedureRequest.occurrenceDateTime;
        if (this.theProcedureRequest.note[0]) {
          this.requestForm.note = this.theProcedureRequest.note[0].text;
        }
        console.log(this.requestForm);

      //});

  }

  // used to search patient when creating a new ProcedureRequest
  getPatientsByName(name: string): void {
    this.patients = [];
    this.patientService.searchPatientsByName(name)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          const next: Patient = Object.assign(new Patient(), entry.resource);
          this.patients.push(next);
        }
        console.log(this.patients);
      });
  }

  getPractitionersByName(name: string): void {
    this.practitioners = [];
    this.practitionerService.getPractitionerByName(name)
      .subscribe(bundle => {
        for (const entry of bundle.entry) {
          //  Push each entry as an Object so as  to access built in class methods
          const next: Practitioner = Object.assign(new Practitioner(), entry.resource)
          this.practitioners.push(next);
        }
        console.log(this.practitioners);
      });
  }

  editProcedureRequest(): void {
    console.log('Input values from Form')
    console.log(this.requestForm);
    const procedureRequest = this.theProcedureRequest;
    procedureRequest.status = this.requestForm.status;
    procedureRequest.intent = this.requestForm.intent;
    procedureRequest.authoredOn = new Date();
    procedureRequest.priority  = this.requestForm.priority;

    this.practitionerService.getPractitionerById(this.requestForm.performer)
      .subscribe(ret => {
        const performer = Object.assign(new Practitioner(), ret);

        procedureRequest.performer.reference = `Practitioner/${performer.id}`;
        procedureRequest.performer.display = performer.getPersonName();

        // assume that requester is the performer
        procedureRequest.requester.agent.reference = `Practitioner/${performer.id}`;
        procedureRequest.requester.agent.display = performer.getPersonName();

        if (this.requestForm.patient.startsWith('Patient/', 0)) {
          procedureRequest.subject.reference = this.requestForm.patient;
        } else {
          procedureRequest.subject.reference = `Patient/${this.requestForm.patient}`;
        }

        this.patientService.getPatientById(this.requestForm.patient)
          .subscribe(resp => {
            const patient: Patient = Object.assign(new Patient(), resp);
            procedureRequest.subject.display = patient.getPersonName();
            console.log("Patients Name: " + patient.getPersonName());
          });
        procedureRequest.doNotPerform = false;
        procedureRequest.occurrenceDateTime = this.requestForm.occurrenceDate;

        // Body Site
        const bodySite = new BodySite();
        bodySite.text = this.requestForm.bodySite;
        procedureRequest.bodySite = [];
        procedureRequest.bodySite.push(bodySite);

        // Coding - set Modality
        const coding = new Coding();
        coding.code = this.requestForm.modality;
        coding.display = 'Modality';
        coding.system = 'http://hl7.org/fhir/v2/0259';
        procedureRequest.code.coding[0] = coding;
        procedureRequest.code.text = 'Modality';

        // Note
        const note = new Annotation();
        note.text = this.requestForm.note;
        if (procedureRequest.note[0]) {
          procedureRequest.note[0] = note;
        } else {
          procedureRequest.note.push(note);
        }

        console.log('Updating procedure-request');
        console.log(procedureRequest);

        this.procedureRequestService.updateProcedureRequest(procedureRequest.id, procedureRequest)
          .subscribe(message => {
            console.log(message);
            this.toaster.success('Procedure Request updated.', 'Success!');
            window.location.reload();
          });
      });
  }

}
