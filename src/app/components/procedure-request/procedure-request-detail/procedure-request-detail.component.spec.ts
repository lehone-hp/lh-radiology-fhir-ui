import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureRequestDetailComponent } from './procedure-request-detail.component';

describe('ProcedureRequestDetailComponent', () => {
  let component: ProcedureRequestDetailComponent;
  let fixture: ComponentFixture<ProcedureRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
