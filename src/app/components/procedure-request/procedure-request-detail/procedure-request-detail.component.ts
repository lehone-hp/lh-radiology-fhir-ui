import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {ProcedureRequestService} from '../../../services/procedure-request.service';
import {PractitionerService} from '../../../services/practitioner.service';
import {PatientService} from '../../../services/patient.service';
import {ActivatedRoute} from '@angular/router';
import {ToastsManager} from 'ng2-toastr';
import {ProcedureRequest} from '../../../models/procedure-request';
import {Patient} from '../../../models/patient';
import {Practitioner} from '../../../models/practitioner';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';

@Component({
  selector: 'app-procedure-request-detail',
  templateUrl: './procedure-request-detail.component.html',
  styleUrls: ['./procedure-request-detail.component.css']
})
export class ProcedureRequestDetailComponent implements OnInit {

  @Input()
  procedureRequest: ProcedureRequest;

  constructor() {}

  ngOnInit() {
  }

}
