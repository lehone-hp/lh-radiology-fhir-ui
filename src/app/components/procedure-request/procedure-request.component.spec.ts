import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureRequestComponent } from './procedure-request.component';

describe('ProcedureRequestComponent', () => {
  let component: ProcedureRequestComponent;
  let fixture: ComponentFixture<ProcedureRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
