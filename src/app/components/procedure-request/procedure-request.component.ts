import {Component, Input, OnInit} from '@angular/core';
import {ProcedureRequest} from '../../models/procedure-request';

@Component({
  selector: 'app-procedure-request',
  templateUrl: './procedure-request.component.html',
  styleUrls: ['./procedure-request.component.css']
})
export class ProcedureRequestComponent implements OnInit {

  @Input()
  procedures: ProcedureRequest[];

  constructor() { }

  ngOnInit() {
  }

  // Opens modal to show procedure request for editing
  showModal(modalId: string): void {
    console.log(`The Element ID: ${modalId}`);
    document.getElementById(modalId).style.display='block';
    document.getElementById(modalId).classList.add('in');
  }

}
