import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DiagnosticReportService} from '../../../services/diagnostic-report.service';
import {DiagnosticReport} from '../../../models/diagnostic-report';
import {Code, Coding} from '../../../models/common';

@Component({
  selector: 'app-report-editor',
  templateUrl: './report-editor.component.html',
  styleUrls: ['./report-editor.component.css']
})
export class ReportEditorComponent implements OnInit {

  @Input()
  report: DiagnosticReport;

  reportText: string;

  router: Router;

  constructor(private reportService: DiagnosticReportService,
              private _router: Router) {
    this.router = this._router;
  }

  ngOnInit() {
    if (this.report.codedDiagnosis) {
      this.reportText = this.report.codedDiagnosis[0].text;
    }
  }

  saveDraftReport(): void {

    console.log(this.reportText);

    this.report.status = 'partial';

    let code = new Code();
    code.text = this.reportText;
    this.report.codedDiagnosis = [];
    this.report.codedDiagnosis.push(code);

    this.reportService.updateReport(this.report.id, this.report)
      .subscribe(resp => {
        console.log(resp)
        this.router.navigate([`/report/${this.report.id}`]);
      });
  }

  saveCompletedReport(): void {
    this.report.status = 'final';

    let code = new Code();
    code.text = this.reportText;
    console.log(this.report);
    this.report.codedDiagnosis = [];
    this.report.codedDiagnosis.push(code);

    this.reportService.updateReport(this.report.id, this.report)
      .subscribe(resp => {
        console.log(resp);
        this.router.navigate([`/report/${this.report.id}`]);
      });
  }
}
