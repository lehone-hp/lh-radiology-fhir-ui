import {Component, Input, OnInit} from '@angular/core';
import {DiagnosticReport} from '../../models/diagnostic-report';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  @Input()
  reports: DiagnosticReport[];

  constructor() { }

  ngOnInit() {
  }

}
