export class Address {
  text: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;
}
