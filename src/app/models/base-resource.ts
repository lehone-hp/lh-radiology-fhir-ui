export class BaseResource {
  resourceType: string;
  id: string;
}
