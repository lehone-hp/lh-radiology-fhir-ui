import {Practitioner} from '../practitioner';

export class PractitionerBundle {
  id: string;
  entry: PractitionerEntry[];
}

export class PractitionerEntry {
  fullUrl: string;
  resource: Practitioner;
}
