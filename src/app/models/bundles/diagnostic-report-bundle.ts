import {BaseResource} from '../base-resource';
import {DiagnosticReport} from '../diagnostic-report';

export class DiagnosticReportBundle extends BaseResource {
  entry: DiagnosticReportEntry[];
}

export class DiagnosticReportEntry {
  fullUrl: string;
  resource: DiagnosticReport;
}
