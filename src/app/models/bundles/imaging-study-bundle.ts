import {ImagingStudy} from '../imaging-study';
import {BaseResource} from '../base-resource';

export class ImagingStudyBundle extends BaseResource {
  entry: ImagingStudyEntry[];
}

export class ImagingStudyEntry {
  fullUrl: string;
  resource: ImagingStudy;
}
