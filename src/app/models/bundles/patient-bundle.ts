import {Patient} from '../patient';

export class PatientBundle {
  id: string;
  entry: PatientBundleEntry[];
}

export class PatientBundleEntry {
  fullUrl: string;
  resource: Patient;
}
