import {ProcedureRequest} from '../procedure-request';

export class ProcedureRequestBundle {
  id: string;
  entry: ProcedureRequestBundleEntry[];
}

export class ProcedureRequestBundleEntry {
  fullUrl: string;
  resource: ProcedureRequest;
}
