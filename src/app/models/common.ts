export class Reference {
  reference: string;
  display: string;
}

export class Code {
  coding: Coding[] = [];
  text: string;
}

export class Coding {
  system: string;
  code: string;
  display: string;
}

export class Annotation {
  authorString: string;
  time: Date;
  text: string;
}

export class ReportPerformer {
  actor: Reference = new Reference;
}
