export interface AppConfig {
  ohif: {
    url: string;
  };

  apiServer: {
    restUrl: string;
  };
}
