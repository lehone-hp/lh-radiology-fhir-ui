import {BaseResource} from './base-resource';
import {Code, Reference, ReportPerformer} from './common';

export class DiagnosticReport extends BaseResource {
  //resourceType: 'DiagnosticReport';
  basedOn: Reference[] = [];
  status: string;
  subject = new Reference;
  performer: ReportPerformer[] = [];
  imagingStudy: Reference[] = [];
  issued: Date;
  conclusion: string;
  codedDiagnosis: Code[] = [];
}
