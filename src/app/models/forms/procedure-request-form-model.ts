export class ProcedureRequestFormModel {
    public patient: string;
    public status: string;
    public intent: string;
    public priority: string;
    public performer: string;
    public bodySite: string;
    public note: string;
    public modality: string;
    public occurrenceDate: Date;
}
