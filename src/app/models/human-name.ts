export class HumanName {
  use: string;
  text: string;
  family: string;
  given: string[];
}
