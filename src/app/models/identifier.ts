export class Identifier {
  system: string;
  value: string;
}
