import {Reference} from './common';
import {ProcedureRequest} from './procedure-request';
import {BaseResource} from './base-resource';

export class ImagingStudy extends BaseResource {
  resourceType = 'ImagingStudy';
  uid: string;
  patient = new Reference();
  referrer = new Reference();
  numberOfSeries: number;
  description: string;
  basedOn: Reference[] = [];
  patientName: string;
  referrerName: string;
  procedureRequest = new ProcedureRequest();
}
