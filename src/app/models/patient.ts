import {Address} from './address';
import {Person} from './person';

export class Patient extends  Person {
  resourceType = 'Patient';
  active: boolean;
  address: Address[];
  deceasedBoolean: boolean;
}
