import {Identifier} from './identifier';
import {HumanName} from './human-name';
import {BaseResource} from './base-resource';

export class Person extends BaseResource {
  identifier: Identifier[] = [];
  name: HumanName[] = [];
  gender: string;
  birthDate: Date;

  public getPersonName(): string {
    let retVal = 'N/A';

    /* if person has a name, use the text for name[0] if available
     * or construct name as family+" "+given[0]
     */
    if (this.name[0]) {
      if (this.name[0].text) {
        retVal = this.name[0].text;
      } else {
        retVal = this.name[0].family;
        if (this.name[0].given[0]) {
          retVal += ` ` + this.name[0].given[0];
        }
      }
    }
    return retVal;
  }
}
