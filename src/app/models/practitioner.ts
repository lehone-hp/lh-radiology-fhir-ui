import {Person} from './person';

export class Practitioner extends Person {
  resourceType: 'Practitioner';
}
