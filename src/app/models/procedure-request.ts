import {Annotation, Code, Coding, Reference} from './common';
import {BaseResource} from './base-resource';

/*Used when making a PUT and POST request*/
export class ProcedureRequestPost extends BaseResource {
  resourceType = 'ProcedureRequest';
  code = new Code();
  status: string;
  intent: string;
  priority: string;
  doNotPerform: boolean;
  subject = new Reference;
  authoredOn = new Date();
  requester = new Requester();
  performer = new Reference();
  bodySite: BodySite[] = [];
  note: Annotation[] = [];
  occurrenceDateTime: Date;
}

/*Used mostly when making GET requests. The patient, practitioner names are set on the fly*/
export class ProcedureRequest extends ProcedureRequestPost {
  patientName: string;
  requesterName: string;
  performerName: string;
}

export class Requester {
  agent = new Reference;
  onBehalfOf = new Reference;
}

export class BodySite {
  coding: Coding[] = [];
  text: string;
}

