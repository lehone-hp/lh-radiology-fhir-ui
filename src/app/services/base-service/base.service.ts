import { Injectable } from '@angular/core';
import {AppConfigService} from '../config/app-config.service';

@Injectable()
export class BaseService {

  public BASEURL: string;

  constructor(private configService: AppConfigService) {
    this.BASEURL = configService.settings.apiServer.restUrl;
    console.log(this.BASEURL);
  }


}
