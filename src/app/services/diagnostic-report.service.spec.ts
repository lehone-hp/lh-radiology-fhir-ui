import { TestBed, inject } from '@angular/core/testing';

import { DiagnosticReportService } from './diagnostic-report.service';

describe('DiagnosticReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiagnosticReportService]
    });
  });

  it('should be created', inject([DiagnosticReportService], (service: DiagnosticReportService) => {
    expect(service).toBeTruthy();
  }));
});
