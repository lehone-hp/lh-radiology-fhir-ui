import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BaseService} from './base-service/base.service';
import {DiagnosticReport} from '../models/diagnostic-report';
import {Observable} from 'rxjs/Observable';
import {DiagnosticReportBundle} from '../models/bundles/diagnostic-report-bundle';
import {AppConfigService} from './config/app-config.service';

@Injectable()
export class DiagnosticReportService extends BaseService {

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
    super(appConfigService);
  }

  url = this.BASEURL + `/DiagnosticReport`;

  getReportById(id: string): Observable<DiagnosticReport> {
    let url: string;
    if (id.startsWith('DiagnosticReport/', 0)) {
      url = this.BASEURL + `/${id}`;
    } else {
      url = this.url+ `/${id}`;
    }
    return this.http.get<DiagnosticReport>(url);
  }

  getAllReports(): Observable<DiagnosticReportBundle> {
    return this.http.get<DiagnosticReportBundle>(this.url);
  }

  getReportByPatientId(patientId: string): Observable<DiagnosticReportBundle> {
    let params = new HttpParams();
    params = params.set('patient', patientId);
    return this.http.get<DiagnosticReportBundle>(this.url, {params});
  }

  getReportByImagingStudy(studyId: string): Observable<DiagnosticReportBundle> {
    let url = this.BASEURL + '/DiagnosticReport';
    let params = new HttpParams();
    params = params.set('image', studyId);
    return this.http.get<DiagnosticReportBundle>(url, {params});
  }

  getReportByProcedureRequest(requestId: string): Observable<DiagnosticReportBundle> {
    let url = this.BASEURL + '/DiagnosticReport';
    let params = new HttpParams();
    params = params.set('based-on', requestId);
    return this.http.get<DiagnosticReportBundle>(url, {params});
  }

  createReport(report: DiagnosticReport): Observable<DiagnosticReport>{
    let url = this.BASEURL + '/DiagnosticReport';
    return this.http.post<DiagnosticReport>(url, report);
  }

  updateReport(id: string, theReport: DiagnosticReport): Observable<DiagnosticReport> {
    let url = this.BASEURL;
    if (id.startsWith('DiagnosticReport/', 0)) {
      url += `/${id}`;
    } else {
      url += `/DiagnosticReport/` + id;
    }
    return this.http.put<DiagnosticReport>(url, theReport);
  }

}
