import { TestBed, inject } from '@angular/core/testing';

import { ImagingStudyService } from './imaging-study.service';

describe('ImagingStudyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImagingStudyService]
    });
  });

  it('should be created', inject([ImagingStudyService], (service: ImagingStudyService) => {
    expect(service).toBeTruthy();
  }));
});
