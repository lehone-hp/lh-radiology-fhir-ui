import { Injectable } from '@angular/core';
import {BaseService} from './base-service/base.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ImagingStudyBundle} from '../models/bundles/imaging-study-bundle';
import {Observable} from 'rxjs/Observable';
import {ImagingStudy} from '../models/imaging-study';
import {AppConfigService} from './config/app-config.service';

@Injectable()
export class ImagingStudyService extends BaseService {

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
    super(appConfigService);
  }

  url = this.BASEURL + `/ImagingStudy`;

  getImagingStudyById(id: string): Observable<ImagingStudy> {
    let url: string;
    if (id.startsWith('ImagingStudy/', 0)) {
      url = this.BASEURL + `/${id}`;
    } else {
      url = this.url + `/${id}`;
    }
    return this.http.get<ImagingStudy>(url);
  }

  getImagingStudies(): Observable<ImagingStudyBundle> {
    return this.http.get<ImagingStudyBundle>(this.url);
  }

  getImagingStudyByPatientId(patientId: string): Observable<ImagingStudyBundle> {
    let params = new HttpParams();
    params = params.set('patient', patientId);
    return this.http.get<ImagingStudyBundle>(this.url, {params});
  }
}
