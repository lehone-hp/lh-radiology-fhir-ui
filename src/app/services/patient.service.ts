import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PatientBundle} from '../models/bundles/patient-bundle';
import {Patient} from '../models/patient';
import {BaseService} from './base-service/base.service';
import {AppConfigService} from './config/app-config.service';

@Injectable()
export class PatientService extends BaseService {

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
    super(appConfigService);
  }

  searchPatientsByName(name: string): Observable<PatientBundle> {
    const url = this.BASEURL + `/Patient`;
    let params = new HttpParams();
    params = params.set('name', name);
    return this.http.get<PatientBundle>(url, {params});
  }

  getPatientById(patientId: string): Observable<Patient> {
    let url: string;
    if (patientId.startsWith('Patient/', 0)) {
      url = this.BASEURL + `/${patientId}`;
    } else {
      url = this.BASEURL + `/Patient/` + patientId;
    }
    return this.http.get<Patient>(url);
  }
}
