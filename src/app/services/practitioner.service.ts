import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Practitioner} from '../models/practitioner';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BaseService} from './base-service/base.service';
import {PractitionerBundle} from '../models/bundles/PractitionerBundle';
import {AppConfigService} from './config/app-config.service';

@Injectable()
export class PractitionerService extends BaseService{

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
    super(appConfigService);
  }

  getPractitionerByName(name: string): Observable<PractitionerBundle> {
    const url = this.BASEURL + `/Practitioner`;
    let params = new HttpParams();
    params = params.set('name', name);
    return this.http.get<PractitionerBundle>(url, {params});
  }

  getPractitionerById(id: string): Observable<Practitioner> {
    let url: string;
    if (id.startsWith('Practitioner/', 0)) {
      url = this.BASEURL + `/${id}`;
    } else {
      url = this.BASEURL + `/Practitioner/` + id;
    }
    return this.http.get<Practitioner>(url);
  }
}
