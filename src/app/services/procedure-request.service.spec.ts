import { TestBed, inject } from '@angular/core/testing';

import { ProcedureRequestService } from './procedure-request.service';

describe('ProcedureRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcedureRequestService]
    });
  });

  it('should be created', inject([ProcedureRequestService], (service: ProcedureRequestService) => {
    expect(service).toBeTruthy();
  }));
});
