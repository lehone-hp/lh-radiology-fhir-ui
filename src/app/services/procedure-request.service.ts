import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ProcedureRequestBundle} from '../models/bundles/procedure-request-bundle';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BaseService} from './base-service/base.service';
import {ProcedureRequest, ProcedureRequestPost} from '../models/procedure-request';
import {AppConfigService} from './config/app-config.service';

@Injectable()
export class ProcedureRequestService extends BaseService {

  constructor(private http: HttpClient,
              private appConfigService: AppConfigService) {
    super(appConfigService);
  }

  getProcedureRequestById(id: string): Observable<ProcedureRequest> {
    let url = this.BASEURL;
    console.log(`The PR id ${id}`);
    if (id.startsWith('ProcedureRequest/', 0)) {
      url += `/${id}`;
    } else {
      url += `/ProcedureRequest/` + id;
    }
    return this.http.get<ProcedureRequest>(url);
  }

  getProceduresByPatientId(patientId: string): Observable<ProcedureRequestBundle> {
    let url = this.BASEURL + '/ProcedureRequest';
    let params = new HttpParams();
    params = params.set('patient', patientId);
    return this.http.get<ProcedureRequestBundle>(url, { params });
  }

  getAllProcedureRequests(): Observable<ProcedureRequestBundle> {
    let url = this.BASEURL + '/ProcedureRequest';
    return this.http.get<ProcedureRequestBundle>(url);
  }

  createProcedureRequest(procedureRequest: ProcedureRequest): Observable<ProcedureRequest>{
    let url = this.BASEURL + '/ProcedureRequest';
    return this.http.post<ProcedureRequest>(url, procedureRequest);
  }

  updateProcedureRequest(id: string, theProcedureRequest: ProcedureRequestPost): Observable<ProcedureRequest> {
    let url = this.BASEURL;
    if (id.startsWith('ProcedureRequest/', 0)) {
      url += `/${id}`;
    } else {
      url += `/ProcedureRequest/` + id;
    }
    return this.http.put<ProcedureRequest>(url, theProcedureRequest);
  }
}
