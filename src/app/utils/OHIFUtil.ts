import {Injectable} from '@angular/core';
import {AppConfigService} from '../services/config/app-config.service';

@Injectable()
export class OHIFUtil {

  constructor(private appConfigService: AppConfigService) {

  }

  generateStudyUrl(studyUid: string): string {
    console.log(`Study uid ${this.appConfigService.settings.ohif.url}/${studyUid}`);
    return `${this.appConfigService.settings.ohif.url}/${studyUid}`;
  }

}
